package com.nlapps.geovideo.Activities;

import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Color;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.View;
import android.view.WindowManager;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.nlapps.geovideo.Adapter.AlbumAdapter;
import com.nlapps.geovideo.Database.DBHelper;
import com.nlapps.geovideo.R;
import com.nlapps.geovideo.Rest.Album;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

public class FileScreenActivity extends AppCompatActivity implements AlbumAdapter.DeleteOptionListener {
    private RecyclerView recyclerView;
    private AlbumAdapter adapter;
    private List<Album> albumList;
    private DBHelper myDb;
    private List<String> sizeList;
    private double total_size;
    private LinearLayout ll_main;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
                WindowManager.LayoutParams.FLAG_FULLSCREEN);
        setContentView(R.layout.activity_file_screen);

        myDb = new DBHelper(this);
        recyclerView = findViewById(R.id.recycler_view);
        ll_main = findViewById(R.id.ll_main);
        TextView textView = findViewById(R.id.text);
        TextView txtViewTotalFiles = findViewById(R.id.txtTotalFiles);
        TextView txtViewTotalSize = findViewById(R.id.txtTotalSize);

        albumList = new ArrayList<>();
        sizeList = new ArrayList<>();

        albumList = myDb.getAllData();
        Cursor cursor = myDb.getAllSize();
        total_size = 0;
        while (cursor.moveToNext()) {
            String a = cursor.getString(cursor.getColumnIndex(DBHelper.SIZE));
            a = a.replace(" MB", "");
            sizeList.add(a);
        }

        for (int i = 0; i < sizeList.size(); i++) {
            total_size = total_size + Double.parseDouble(sizeList.get(i));
        }
        String size_in_mbs = String.format("%.2f", total_size);
        txtViewTotalFiles.setText(albumList.size() + " Files");
        txtViewTotalSize.setText("Size " + size_in_mbs + " (MBs)");

        if (albumList.size() <= 0) {
            textView.setVisibility(View.VISIBLE);
            recyclerView.setVisibility(View.GONE);
            return;
        }
        adapter = new AlbumAdapter(this, albumList, this);

        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(this);
        recyclerView.setLayoutManager(mLayoutManager);
        recyclerView.setItemAnimator(new DefaultItemAnimator());
        recyclerView.setAdapter(adapter);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        startActivity(new Intent(this, Home.class));
        finish();
    }

    private void showSnackBar(String message) {
        Snackbar snackbar = Snackbar.make(ll_main, message, Snackbar.LENGTH_LONG);
        snackbar.setActionTextColor(Color.YELLOW);
        snackbar.show();
    }

    @Override
    public void deleteItem(int position) {

        final Album item = adapter.getData().get(position);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Are you sure you want to delete the " + item.name + " video");
        builder.setPositiveButton("Yes", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                deleteItem(position,item);
                dialogInterface.dismiss();
            }
        }).setNegativeButton("No", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialogInterface, int i) {
                dialogInterface.dismiss();
            }
        }).create();
        builder.show();
    }

    private void deleteItem(int position,Album item){
        if (myDb.deleteItem(item.id)) {
            adapter.removeItem(position);
            String path = item.getVideo_url().substring(7, item.getVideo_url().length());
            File deleteMp4 = new File(path);
            File deleteCsv = new File(path + ".csv");
            File deleteXls = new File(path + ".xls");
            if (deleteMp4.exists()) {
                if (deleteMp4.delete()) {
                    deleteCsv.delete();
                    deleteXls.delete();
                    showSnackBar("Item Deleted Successfully");
                } else {
                    showSnackBar("Unable to delete file");
                }
            } else {
                showSnackBar("File Does't Exist");
            }
        } else {
            showSnackBar("Item not exist in DB");
        }
    }
}